package id.co.asyst.amala.member.buymileage.controller.buymileage;


import id.co.asyst.amala.member.buymileage.service.buymileage.BuyMileageService;
import id.co.asyst.amala.member.buymileage.validator.buymileage.BuyMileageValidator;
import id.co.asyst.amala.member.model.BuyMileage;
import id.co.asyst.commons.core.component.Translator;
import id.co.asyst.commons.core.controller.BaseController;
import id.co.asyst.commons.core.exception.ApplicationException;
import id.co.asyst.commons.core.exception.SystemsException;
import id.co.asyst.commons.core.payload.*;
import id.co.asyst.commons.core.validator.ValidatorType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.MDC;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/buymileage/v1.2")
public class BuyMileageController extends BaseController {

    private static final long serialVersionUID = -4378005893398623600L;

    private static Logger logger = LogManager.getLogger();


    private BuyMileageService buyMileageService;
    private BuyMileageValidator validator;

    public BuyMileageController(BuyMileageService buyMileageService, BuyMileageValidator validator) {
        this.buyMileageService = buyMileageService;
        this.validator = validator;
    }

//    @PostMapping("${controller.mapping.operation.retrievedetail}")
//    public BaseResponse<BuyMileage> retrievedetail(@Valid @RequestBody BaseRequest<BaseParameter<BuyMileage>> request) {
//        BaseResponse<BuyMileage> response = new BaseResponse<>();
//        try {
//            response.setIdentity(request.getIdentity());
//            BuyMileage data = request.getParameter().getData();
//            validator.validate(request, ValidatorType.RETRIEVEDETAIL);
//            response.setIdentity(request.getIdentity());
//            response.setResult(buyMileageService.retrievedetail(data, request.getIdentity()));
//            response.setStatus(Status.SUCCESS());
//        } catch (ApplicationException ve) {
//            response.setStatus(ve.getStatus());
//        } catch (DataIntegrityViolationException he) {
//            response.setStatus(new Status(Status.ERROR_INVALID_DATA_CODE, Status.ERROR_INVALID_DATA_DESC,
//                    Translator.get("data.constraint.violation")));
//        } catch (Exception e) {
//            response.setStatus(Status.ERROR(e.getMessage()));
//        }
//        return response;
//    }

    @PostMapping("${controller.mapping.operation.create}")
    public BaseResponse<BuyMileage> create(@Valid @RequestBody BaseRequest<BaseParameter<BuyMileage>> request) {
        BaseResponse<BuyMileage> response = new BaseResponse<BuyMileage>();
        response.setIdentity(request.getIdentity());
        try {
            validator.validate(request, ValidatorType.CREATE);
            response.setResult(buyMileageService.save(request.getParameter().getData(), request.getIdentity()));
            response.setStatus(Status.SUCCESS(Translator.get("buymileagelimit.success.create")));
        } catch (ApplicationException | SystemsException e) {
            if (e.getStatus() != null) {
                response.setStatus(e.getStatus());
            } else {
                response.setStatus(Status.INVALID(Translator.get(e.getKey(), e.getModuleName(), e.getParameter())));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
        }
        return response;
    }


    @PostMapping("${controller.mapping.operation.update}")
    public BaseResponse<BuyMileage> update(@Valid @RequestBody BaseRequest<BaseParameter<BuyMileage>> request) {
        BaseResponse<BuyMileage> response = new BaseResponse<>();
        response.setIdentity(request.getIdentity());
        try {
            validator.validate(request, ValidatorType.UPDATE);
            response.setResult(buyMileageService.update(request.getParameter().getData(), request.getIdentity()));
            response.setStatus(Status.SUCCESS(Translator.get("buymileage.success.update")));
        } catch (ApplicationException | SystemsException e) {
            if (e.getStatus() != null) {
                response.setStatus(e.getStatus());
            } else {
                response.setStatus(Status.INVALID(Translator.get(e.getKey(), e.getModuleName(), e.getParameter())));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
        }
        return response;
    }

    @PostMapping("${controller.mapping.operation.retrieve}")
    public BaseResponse<List<BuyMileage>> retrieve(@RequestBody BaseRequest<BaseParameter<BuyMileage>> request) {
        BaseResponse<List<BuyMileage>> response = new BaseResponse<List<BuyMileage>>();
        try {
            // Parse Request
            Identity identity = request.getIdentity();
            response.setIdentity(identity);
            // Validate Request
            Paging paging = (request.getPaging() == null) ? Paging.initialize() : Paging.validate(request.getPaging());
            BaseParameter<BuyMileage> parameter = request.getParameter();
            validator.validate(parameter);
            List<BuyMileage> buyMileageList = null;
            if (paging.getLimit() < 0) {
                if (parameter != null) {
                    buyMileageList = buyMileageService.executeCustomSelectQuery(parameter, BuyMileage.class, true, true);
                } else {
                    buyMileageList = buyMileageService.findAll();
                }
            } else {
                Page<BuyMileage> buyMileagePage;
                if (parameter != null) {
                    buyMileagePage = buyMileageService.executeCustomSelectQuery(parameter, paging, BuyMileage.class, true, true);
                    buyMileageList = buyMileagePage.getContent();
                } else {
                    buyMileagePage = buyMileageService.findAll(paging);
                    buyMileageList = buyMileagePage.getContent();
                }
                paging.setTotalpage(buyMileagePage.getTotalPages());
                paging.setTotalrecord(buyMileagePage.getTotalElements());
                response.setPaging(paging);
            }
            // Set Response Result
            response.setResult(buyMileageList);
            // Set Response Status
            response.setStatus(new Status(Status.SUCCESS_CODE, Status.SUCCESS_DESC));
        } catch (ApplicationException | SystemsException e) {
            response.setStatus(e.getStatus());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
        }
        return response;
    }

    @PostMapping("${controller.mapping.operation.delete}")
    public BaseResponse<BuyMileage> delete(@RequestBody BaseRequest<BaseParameter<BuyMileage>> request) {
        BaseResponse<BuyMileage> response = new BaseResponse();
        BuyMileage fileDataRequest = request.getParameter().getData();
        response.setIdentity(request.getIdentity());
        try {
            validator.validate(request, ValidatorType.DELETE);
            buyMileageService.delete(fileDataRequest);
            response.setStatus(Status.SUCCESS(Translator.get("buymileage.success.delete")));
        } catch (ApplicationException | SystemsException e) {
            if (e.getStatus() != null) {
                response.setStatus(e.getStatus());
            } else {
                response.setStatus(Status.INVALID(Translator.get(e.getKey(), e.getModuleName(), e.getParameter())));
            }
        } catch (DataIntegrityViolationException he) {
            response.setStatus(Status.INVALID(Translator.get("data.not.delete", "Buy Mileage Limit", request.getParameter().getData().getMemberbuymileageid())));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
        }
        return response;
    }

}
