package id.co.asyst.amala.member.buymileage.controller.buymileagelimit;

import id.co.asyst.amala.member.buymileage.service.buymileagelimit.BuyMileageLimitService;
import id.co.asyst.amala.member.buymileage.validator.buymileagelimit.BuyMileageLimitValidator;
import id.co.asyst.amala.member.model.BuyMileageLimit;
import id.co.asyst.commons.core.component.Translator;
import id.co.asyst.commons.core.controller.BaseController;
import id.co.asyst.commons.core.exception.ApplicationException;
import id.co.asyst.commons.core.exception.SystemsException;
import id.co.asyst.commons.core.payload.*;
import id.co.asyst.commons.core.validator.ValidatorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/buymileagelimit/v1.2")
public class BuyMileageLimitController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(BuyMileageLimitController.class);

    private BuyMileageLimitService buyMileageLimitService;
    private BuyMileageLimitValidator buyMileageLimitValidator;

    @Autowired
    public BuyMileageLimitController(BuyMileageLimitService buyMileageLimitService, BuyMileageLimitValidator buyMileageLimitValidator) {
        this.buyMileageLimitService = buyMileageLimitService;
        this.buyMileageLimitValidator = buyMileageLimitValidator;
    }

    @PostMapping("/create")
    public BaseResponse<BuyMileageLimit> create(@Valid @RequestBody BaseRequest<BaseParameter<BuyMileageLimit>> request) {
        BaseResponse<BuyMileageLimit> response = new BaseResponse<BuyMileageLimit>();
        response.setIdentity(request.getIdentity());
        try {
            buyMileageLimitValidator.validate(request, ValidatorType.CREATE);
            response.setResult(buyMileageLimitService.save(request.getParameter().getData(), request.getIdentity()));
            response.setStatus(Status.SUCCESS(Translator.get("buymileagelimit.success.create")));
        } catch (ApplicationException | SystemsException e) {
            if (e.getStatus() != null) {
                response.setStatus(e.getStatus());
            } else {
                response.setStatus(Status.INVALID(Translator.get(e.getKey(), e.getModuleName(), e.getParameter())));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
        }
        return response;
    }

    @PostMapping("/update")
    public BaseResponse<BuyMileageLimit> update(@Valid @RequestBody BaseRequest<BaseParameter<BuyMileageLimit>> request) {
        BaseResponse<BuyMileageLimit> response = new BaseResponse<>();
        response.setIdentity(request.getIdentity());
        try {
            buyMileageLimitValidator.validate(request, ValidatorType.UPDATE);
            response.setResult(buyMileageLimitService.update(request.getParameter().getData(), request.getIdentity()));
            response.setStatus(Status.SUCCESS(Translator.get("buymileagelimit.success.update")));
        } catch (ApplicationException | SystemsException e) {
            if (e.getStatus() != null) {
                response.setStatus(e.getStatus());
            } else {
                response.setStatus(Status.INVALID(Translator.get(e.getKey(), e.getModuleName(), e.getParameter())));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
        }
        return response;
    }

    @PostMapping("/retrieve")
    public BaseResponse<List<BuyMileageLimit>> retrieve(@RequestBody BaseRequest<BaseParameter<BuyMileageLimit>> request) {
        BaseResponse<List<BuyMileageLimit>> response = new BaseResponse<>();
        response.setIdentity(request.getIdentity());
        try {
            BaseParameter<BuyMileageLimit> parameter = request.getParameter();
            buyMileageLimitValidator.validate(parameter);
            Paging paging = (request.getPaging() == null) ? Paging.initialize() : Paging.validate(request.getPaging());
            List<BuyMileageLimit> buyMileageLimitList;
            if (paging.getLimit() < 0) {
                if (parameter != null) {
                    buyMileageLimitList = buyMileageLimitService.executeCustomSelectQuery(parameter, BuyMileageLimit.class, true, true);
                } else {
                    buyMileageLimitList = buyMileageLimitService.findAll();
                }
            } else {
                Page<BuyMileageLimit> buyMileageLimitPage;
                if (parameter != null) {
                    buyMileageLimitPage = buyMileageLimitService.executeCustomSelectQuery(parameter, paging, BuyMileageLimit.class, true, true);
                    buyMileageLimitList = buyMileageLimitPage.getContent();
                } else {
                    buyMileageLimitPage = buyMileageLimitService.findAll(paging);
                    buyMileageLimitList = buyMileageLimitPage.getContent();
                }
                paging.setTotalpage(buyMileageLimitPage.getTotalPages());
                paging.setTotalrecord(buyMileageLimitPage.getTotalElements());
                response.setPaging(paging);
            }
            // Set Response Result
            response.setResult(buyMileageLimitList);
            response.setStatus(Status.SUCCESS());
        } catch (ApplicationException | SystemsException e) {
            if (e.getStatus() != null) {
                response.setStatus(e.getStatus());
            } else {
                response.setStatus(Status.INVALID(Translator.get(e.getKey(), e.getModuleName(), e.getParameter())));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
        }
        return response;
    }

    @PostMapping("/delete")
    public BaseResponse<BuyMileageLimit> delete(@RequestBody BaseRequest<BaseParameter<BuyMileageLimit>> request) {
        BaseResponse<BuyMileageLimit> response = new BaseResponse();
        BuyMileageLimit fileDataRequest = request.getParameter().getData();
        response.setIdentity(request.getIdentity());
        try {
            buyMileageLimitValidator.validate(request, ValidatorType.DELETE);
            buyMileageLimitService.delete(fileDataRequest);
            response.setStatus(Status.SUCCESS(Translator.get("buymileagelimit.success.delete")));
        } catch (ApplicationException | SystemsException e) {
            if (e.getStatus() != null) {
                response.setStatus(e.getStatus());
            } else {
                response.setStatus(Status.INVALID(Translator.get(e.getKey(), e.getModuleName(), e.getParameter())));
            }
        } catch (DataIntegrityViolationException he) {
            response.setStatus(Status.INVALID(Translator.get("data.not.delete", "Buy Mileage Limit", request.getParameter().getData().getMemberbuymileagelimitid())));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
        }
        return response;
    }

    @PostMapping("/collect")
    public BaseResponse<BuyMileageLimit> collect(@Valid @RequestBody BaseRequest<BaseParameter<BuyMileageLimit>> request) {
        BaseResponse<BuyMileageLimit> response = new BaseResponse<>();
        response.setIdentity(request.getIdentity());
        try {
            buyMileageLimitValidator.validateCollect(request);
            response.setResult(buyMileageLimitService.collect(request.getParameter().getData(), request.getIdentity()));
            response.setStatus(Status.SUCCESS(Translator.get("buymileagelimit.success.collect")));
        } catch (ApplicationException | SystemsException e) {
            if (e.getStatus() != null) {
                response.setStatus(e.getStatus());
            } else {
                response.setStatus(Status.INVALID(Translator.get(e.getKey(), e.getModuleName(), e.getParameter())));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
        }
        return response;
    }

    @PostMapping("/check")
    public BaseResponse<BuyMileageLimit> check(@Valid @RequestBody BaseRequest<BaseParameter<BuyMileageLimit>> request) {
        BaseResponse<BuyMileageLimit> response = new BaseResponse<>();
        response.setIdentity(request.getIdentity());
        try {
            buyMileageLimitValidator.validateCheck(request);
            response.setResult(buyMileageLimitService.check(request.getParameter().getData(), request.getIdentity()));
            response.setStatus(Status.SUCCESS());
        } catch (ApplicationException | SystemsException e) {
            if (e.getStatus() != null) {
                response.setStatus(e.getStatus());
            } else {
                response.setStatus(Status.INVALID(Translator.get(e.getKey(), e.getModuleName(), e.getParameter())));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
        }
        return response;
    }

//    @PostMapping("/retrievedetail")
//    public BaseResponse<BuyMileageLimit> retrievedetail(@Valid @RequestBody BaseRequest<BaseParameter<BuyMileageLimit>> request) {
//        BaseResponse<BuyMileageLimit> response = new BaseResponse<BuyMileageLimit>();
//        response.setIdentity(request.getIdentity());
//        try {
//            buyMileageLimitValidator.validate(request, ValidatorType.RETRIEVEDETAIL);
//            response.setResult(buyMileageLimitService.retrievedetail(request.getParameter().getData(), request.getIdentity()));
//            response.setStatus(Status.SUCCESS());
//        } catch (ApplicationException | SystemsException e) {
//            if (e.getStatus() != null) {
//                response.setStatus(e.getStatus());
//            } else {
//                response.setStatus(Status.INVALID(Translator.get(e.getKey(), e.getModuleName(), e.getParameter())));
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//            response.setStatus(Status.ERROR(MDC.get("X-B3-TraceId") + " - " + MDC.get("X-B3-SpanId")));
//        }
//        return response;
//    }


}
