package id.co.asyst.amala.member.buymileage.repository.buymileage;

import id.co.asyst.amala.member.model.BuyMileage;
import id.co.asyst.commons.core.repository.BaseRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BuyMileageRepository extends BaseRepository<BuyMileage, String>, BuyMileageRepositoryCustom {

    BuyMileage findByMemberbuymileageid(String memberbuymileageid);
}
