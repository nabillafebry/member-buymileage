package id.co.asyst.amala.member.buymileage.repository.buymileage;

import id.co.asyst.amala.member.model.BuyMileage;
import id.co.asyst.commons.core.repository.RepositoryCustom;
import org.springframework.stereotype.Repository;

@Repository
public interface BuyMileageRepositoryCustom extends RepositoryCustom<BuyMileage> {

}
