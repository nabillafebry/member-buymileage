package id.co.asyst.amala.member.buymileage.repository.buymileage;

import id.co.asyst.amala.member.model.BuyMileage;
import id.co.asyst.commons.core.repository.BaseRepositoryCustom;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class BuyMileageRepositoryCustomImpl extends BaseRepositoryCustom<BuyMileage> implements BuyMileageRepositoryCustom {

    private static final long serialVersionUID = 8972708733606285338L;

    @Override
    public Map<String, String> requestKeyToColumnNameMapping() {
        Map<String, String> keyMaps = new HashMap<>();
        keyMaps.put("memberid", "member.memberid");
        keyMaps.put("partnercode", "partner.partnercode");
        keyMaps.put("currencycode", "currency.currencycode");
        keyMaps.put("trxid", "transaction.trxid");
        keyMaps.put("buymileageid", "buymileagecatalog.buymileageid");
        keyMaps.put("promoid", "promo.promoid");
        return keyMaps;
    }

}
