package id.co.asyst.amala.member.buymileage.repository.buymileagelimit;

import id.co.asyst.amala.member.model.BuyMileageLimit;
import id.co.asyst.commons.core.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BuyMileageLimitRepository extends BaseRepository<BuyMileageLimit, String>, BuyMileageLimitRepositoryCustom {

    @Query("SELECT a FROM BuyMileageLimit a WHERE a.member.memberid = ?1")
    List<BuyMileageLimit> findByMemberid(String memberid);
}
