package id.co.asyst.amala.member.buymileage.repository.buymileagelimit;

import id.co.asyst.amala.member.model.BuyMileageLimit;
import id.co.asyst.commons.core.repository.RepositoryCustom;

public interface BuyMileageLimitRepositoryCustom extends RepositoryCustom<BuyMileageLimit> {
}
