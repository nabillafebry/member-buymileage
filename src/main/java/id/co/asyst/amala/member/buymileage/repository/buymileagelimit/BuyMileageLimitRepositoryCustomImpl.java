package id.co.asyst.amala.member.buymileage.repository.buymileagelimit;

import id.co.asyst.amala.member.model.BuyMileageLimit;
import id.co.asyst.commons.core.repository.BaseRepositoryCustom;

import java.util.HashMap;
import java.util.Map;

public class BuyMileageLimitRepositoryCustomImpl extends BaseRepositoryCustom<BuyMileageLimit> implements BuyMileageLimitRepositoryCustom {
    @Override
    public Map<String, String> requestKeyToColumnNameMapping() {
        Map<String, String> keyMaps = new HashMap<>();
        keyMaps.put("memberid", "member.memberid");
        return keyMaps;
    }
}
