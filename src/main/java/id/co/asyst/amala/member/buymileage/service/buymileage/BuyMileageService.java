package id.co.asyst.amala.member.buymileage.service.buymileage;

import id.co.asyst.amala.member.model.BuyMileage;
import id.co.asyst.commons.core.payload.Identity;
import id.co.asyst.commons.core.service.Service;

import java.text.ParseException;

public interface BuyMileageService extends Service<BuyMileage, String> {

    BuyMileage save(BuyMileage buyMileage, Identity identity) throws ParseException;

    BuyMileage update(BuyMileage buyMileage, Identity identity);

    void delete(BuyMileage buyMileage);

}
