package id.co.asyst.amala.member.buymileage.service.buymileage;

import com.google.gson.Gson;
import id.co.asyst.amala.buymileage.model.BuyMileageMaster;
import id.co.asyst.amala.buymileage.model.BuyMileagePromo;
import id.co.asyst.amala.feign.*;
import id.co.asyst.amala.master.currency.model.Currency;
import id.co.asyst.amala.member.buymileage.repository.buymileage.BuyMileageRepository;
import id.co.asyst.amala.member.model.BuyMileage;
import id.co.asyst.amala.member.model.Member;
import id.co.asyst.amala.member.model.Transaction;
import id.co.asyst.amala.partner.model.Partner;
import id.co.asyst.commons.core.component.Translator;
import id.co.asyst.commons.core.exception.ApplicationException;
import id.co.asyst.commons.core.payload.*;
import id.co.asyst.commons.core.service.BaseService;
import id.co.asyst.commons.core.utils.DateUtils;
import id.co.asyst.commons.core.utils.GeneratorUtils;
import id.co.asyst.commons.core.validator.ValidatorType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
@Transactional
public class BuyMileageServiceimpl extends BaseService<BuyMileage, String> implements BuyMileageService {

    Logger logger = LogManager.getLogger();

    private BuyMileageRepository buyMileageRepository;
    private MemberProfileClient memberProfileClient;
    private PartnerClient partnerClient;
    private CurrencyClient currencyClient;
    private BuyMileageMasterClient buyMileageMasterClient;
    private MemberTransactionClient memberTransactionClient;
    private BuyMileagePromoClient buyMileagePromoClient;

    @Autowired
    public BuyMileageServiceimpl(BuyMileageRepository buyMileageRepository, MemberProfileClient memberProfileClient,
                                 PartnerClient partnerClient, CurrencyClient currencyClient,
                                 BuyMileageMasterClient buyMileageMasterClient, MemberTransactionClient memberTransactionClient,
                                 BuyMileagePromoClient buyMileagePromoClient) {
        this.partnerClient = partnerClient;
        this.currencyClient = currencyClient;
        this.buyMileageMasterClient = buyMileageMasterClient;
        this.memberProfileClient = memberProfileClient;
        this.buyMileageRepository = buyMileageRepository;
        this.memberTransactionClient = memberTransactionClient;
        this.buyMileagePromoClient = buyMileagePromoClient;
        this.setRepository(buyMileageRepository);
        this.setCustomRepository(buyMileageRepository);
    }


    @Override
    public BuyMileage save(BuyMileage buyMileage, Identity identity) {
        String memberbuymileageid = GeneratorUtils.GenerateId("", DateUtils.now(), 5);
        buyMileage.setMemberbuymileageid(memberbuymileageid);
        checkMemberBuyMileage(buyMileage, ValidatorType.CREATE);

        checkDependencies(buyMileage, identity);

        int qty = buyMileage.getQty();
        int basemileage = buyMileage.getBuymileagecatalog().getBasemileage();

        int totalmileage = qty * basemileage;

        buyMileage.setTotalmileage(totalmileage);

        buyMileage.setCreatedBy(identity.getUserid());
        super.save(buyMileage);
        return buyMileage;
    }


    @Override
    public BuyMileage update(BuyMileage buyMileage, Identity identity) {
        BuyMileage buyMileageDB = checkMemberBuyMileage(buyMileage, ValidatorType.UPDATE);

        checkDependencies(buyMileage, identity);

        int qty = buyMileage.getQty();
        int basemileage = buyMileage.getBuymileagecatalog().getBasemileage();

        int totalmileage = qty * basemileage;

        buyMileage.setTotalmileage(totalmileage);

        buyMileage.setUpdatedBy(identity.getUserid());
        buyMileage.setCreatedBy(buyMileageDB.getCreatedBy());
        super.update(buyMileage);
        return buyMileage;
    }

    @Override
    public void delete(BuyMileage buyMileage) {
        BuyMileage buyMileageDB = buyMileageRepository.findById(buyMileage.getMemberbuymileageid()).orElse(null);

        if (buyMileageDB != null) {
            try {
                this.delete(buyMileage.getMemberbuymileageid());
            } catch (Exception e) {
                throw new ApplicationException("data.not.delete", "Buy Mileage ", buyMileage.getMemberbuymileageid());
            }
        } else {
            throw new ApplicationException("data.not.found", "Buy Mileage ", buyMileage.getMemberbuymileageid());
        }
    }

    private BuyMileage checkMemberBuyMileage(BuyMileage buyMileage, ValidatorType type) {
        BuyMileage buyMileageDB = buyMileageRepository.findById(buyMileage.getMemberbuymileageid()).orElse(null);
        if (!ValidatorType.CREATE.equals(type)) {
            if (buyMileageDB != null) {
                return buyMileageDB;
            } else {
                throw new ApplicationException("data.not.found", "Member Buy Mileage", buyMileage.getMemberbuymileageid());
            }
        } else {
            if (buyMileageDB != null) {
                throw new ApplicationException("data.already.exist", "Member Buy Mileage", buyMileage.getMemberbuymileageid());
            } else {
                return buyMileage;
            }
        }
    }

    private void checkDependencies(BuyMileage buyMileage, Identity identity) {
        // check buymileage master
        BaseRequest<BaseParameter> reqBuyMileageMaster = reqBuyMileageMaster(buyMileage.getBuymileageid(), identity);
        BaseResponse<List<BuyMileageMaster>> buymileagemaster = buyMileageMasterClient.retrieve(reqBuyMileageMaster);
        logger.info("MASTER::"+ new Gson().toJson(buymileagemaster));
        if (buymileagemaster != null && buymileagemaster.getResult() != null && buymileagemaster.getResult().size() > 0) {
            buyMileage.setBuymileagecatalog(buymileagemaster.getResult().get(0));
        } else {
            throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Buy Mileage", buyMileage.getBuymileageid())));
        }

        // check member
        BaseRequest<BaseParameter<Member>> reqMember = reqMember(buyMileage.getMemberid(), identity);
        BaseResponse<Member> member = memberProfileClient.profile(reqMember);
        if (member != null && member.getResult() != null) {
            buyMileage.setMember(member.getResult());
        } else {
            throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Member", buyMileage.getMemberid())));
        }

        // check currency
        BaseRequest<BaseParameter> reqCurrency = reqCurrency(buyMileage.getCurrencycode(), identity);
        BaseResponse<List<Currency>> currency = currencyClient.retrieve(reqCurrency);
        if (currency != null && currency.getResult() != null && currency.getResult().size() > 0) {
            buyMileage.setCurrency(currency.getResult().get(0));
        } else {
            throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Currency", buyMileage.getCurrencycode())));
        }

        // check partner
        if (!StringUtils.isEmpty(buyMileage.getPartnercode())) {
            BaseRequest<BaseParameter> reqPartner = reqPartner(buyMileage.getPartnercode(), identity);
            BaseResponse<List<Partner>> partner = partnerClient.retrieve(reqPartner);
            if (partner != null && partner.getResult() != null && partner.getResult().size() > 0) {
                buyMileage.setPartner(partner.getResult().get(0));
            } else {
                throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Partner", buyMileage.getPartnercode())));
            }
        }

        // check trx
        if (!StringUtils.isEmpty(buyMileage.getTrxid())) {
            BaseRequest<BaseParameter<Transaction>> reqTransaction = reqTransaction(buyMileage.getTrxid(), identity);
            BaseResponse<List<Transaction>> trx = memberTransactionClient.retrieve(reqTransaction);
            if (trx != null && trx.getResult() != null && trx.getResult().size() > 0) {
                buyMileage.setTransaction(trx.getResult().get(0));
            } else {
                throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Transaction", buyMileage.getTrxid())));
            }
        }

        //check promo
        if (!StringUtils.isEmpty(buyMileage.getPromoid())) {
            BaseRequest<BaseParameter> reqBuyMileagePromo = reqBuyMileagePromo(buyMileage.getPromoid(), identity);
            BaseResponse<List<BuyMileagePromo>> buymileagepromo = buyMileagePromoClient.retrieve(reqBuyMileagePromo);
            if (buymileagepromo != null && buymileagepromo.getResult() != null && buymileagepromo.getResult().size() > 0) {
                buyMileage.setPromo(buymileagepromo.getResult().get(0));
            } else {
                throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Buy Mileage Promo", buyMileage.getPromoid())));
            }
        }
    }

    public BaseRequest reqMember(String memberid, Identity identity) {
        BaseParameter<Member> parameter = new BaseParameter<>();
        BaseRequest<BaseParameter> request = new BaseRequest<>();
        Member member = new Member();
        member.setMemberid(memberid);
        parameter.setData(member);
        request.setParameter(parameter);
        request.setIdentity(identity);
        return request;
    }

    public BaseRequest reqPartner(String partnercode, Identity identity) {
        BaseParameter<Partner> parameter = new BaseParameter<>();
        BaseRequest<BaseParameter> request = new BaseRequest<>();
        parameter.addCriteria("partnercode", partnercode);
        request.setParameter(parameter);
        request.setIdentity(identity);
        return request;
    }

    public BaseRequest reqCurrency(String currencycode, Identity identity) {
        BaseParameter<Currency> parameter = new BaseParameter<>();
        BaseRequest<BaseParameter> request = new BaseRequest<>();
        parameter.addCriteria("currencycode", currencycode);
        request.setParameter(parameter);
        request.setIdentity(identity);
        return request;
    }

    public BaseRequest reqBuyMileageMaster(String buymileageid, Identity identity) {
        BaseParameter<BuyMileageMaster> parameter = new BaseParameter<>();
        BaseRequest<BaseParameter> request = new BaseRequest<>();
        parameter.addCriteria("buymileageid", buymileageid);
        request.setParameter(parameter);
        request.setIdentity(identity);
        logger.info("REQ MASTER::"+ new Gson().toJson(request));
        return request;
    }

    public BaseRequest reqBuyMileagePromo(String promoid, Identity identity) {
        BaseParameter<BuyMileagePromo> parameter = new BaseParameter<>();
        BaseRequest<BaseParameter> request = new BaseRequest<>();
        parameter.addCriteria("promoid", promoid);
        request.setParameter(parameter);
        request.setIdentity(identity);
        return request;
    }

    public BaseRequest reqTransaction(String trxid, Identity identity) {
        BaseParameter<Transaction> parameter = new BaseParameter<>();
        BaseRequest<BaseParameter> request = new BaseRequest<>();
        parameter.addCriteria("trxid", trxid);
        request.setParameter(parameter);
        request.setIdentity(identity);
        return request;
    }
}