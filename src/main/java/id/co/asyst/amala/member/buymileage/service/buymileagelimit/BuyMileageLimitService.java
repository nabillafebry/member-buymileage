package id.co.asyst.amala.member.buymileage.service.buymileagelimit;

import id.co.asyst.amala.member.model.BuyMileageLimit;
import id.co.asyst.commons.core.payload.Identity;
import id.co.asyst.commons.core.service.Service;

import java.text.ParseException;

public interface BuyMileageLimitService extends Service<BuyMileageLimit, String> {

    BuyMileageLimit save(BuyMileageLimit buyMileageLimit, Identity identity) throws ParseException;

    BuyMileageLimit update(BuyMileageLimit buyMileageLimit, Identity identity);

    BuyMileageLimit collect(BuyMileageLimit buyMileageLimit, Identity identity) throws ParseException;

    BuyMileageLimit check(BuyMileageLimit buyMileageLimit, Identity identity);

    void delete(BuyMileageLimit buyMileageLimit);

}
