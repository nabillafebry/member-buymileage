package id.co.asyst.amala.member.buymileage.service.buymileagelimit;

import id.co.asyst.amala.feign.GeneralConfigurationClient;
import id.co.asyst.amala.feign.MemberProfileClient;
import id.co.asyst.amala.master.generalconfiguration.model.GeneralConfiguration;
import id.co.asyst.amala.member.buymileage.repository.buymileagelimit.BuyMileageLimitRepository;
import id.co.asyst.amala.member.model.BuyMileageLimit;
import id.co.asyst.amala.member.model.Member;
import id.co.asyst.commons.core.component.Translator;
import id.co.asyst.commons.core.exception.ApplicationException;
import id.co.asyst.commons.core.payload.*;
import id.co.asyst.commons.core.service.BaseService;
import id.co.asyst.commons.core.utils.DateUtils;
import id.co.asyst.commons.core.utils.GeneratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class BuyMileageLimitServiceImpl extends BaseService<BuyMileageLimit, String> implements BuyMileageLimitService {

    private BuyMileageLimitRepository buyMileageLimitRepository;
    private MemberProfileClient memberProfileClient;
    private GeneralConfigurationClient generalConfigurationClient;

    @Autowired
    public BuyMileageLimitServiceImpl(BuyMileageLimitRepository buyMileageLimitRepository, MemberProfileClient memberProfileClient,
                                      GeneralConfigurationClient generalConfigurationClient) {
        this.buyMileageLimitRepository = buyMileageLimitRepository;
        this.memberProfileClient = memberProfileClient;
        this.generalConfigurationClient = generalConfigurationClient;
        this.setRepository(buyMileageLimitRepository);
        this.setCustomRepository(buyMileageLimitRepository);
    }

    @Override
    public BuyMileageLimit save(BuyMileageLimit buyMileageLimit, Identity identity) throws ParseException {
        String memberbuymileagelimitid = GeneratorUtils.GenerateId("", DateUtils.now(), 5);
        // check member
        BaseRequest<BaseParameter<Member>> reqMember = reqMember(buyMileageLimit.getMemberid(), identity);
        BaseResponse<Member> member = memberProfileClient.profile(reqMember);
        if (!member.getStatus().getResponsecode().equals("0000")) {
            throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Member", buyMileageLimit.getMemberid())));
        }

        if (buyMileageLimit.getStartdate() == null) {
            // check general config
            BaseRequest<BaseParameter> reqGeneral = reqGeneral("buymileage.limit.period.start", identity);
            BaseResponse<List<GeneralConfiguration>> general = generalConfigurationClient.retrieve(reqGeneral);
            if (general.getResult().size() < 0) {
                throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "General Config", "buymileage.limit.period.start")));
            }
            if (general.getResult().get(0).getValue().equals("YEAR_START")) {
                Calendar cal = Calendar.getInstance();
                int year = Calendar.getInstance().get(Calendar.YEAR);
                cal.set(year, 0, 1);
                Date now = cal.getTime();
                buyMileageLimit.setStartdate(now);
            } else if (general.getResult().get(0).getValue().equals("DATE")) {
                // check general config
                BaseRequest<BaseParameter> reqGeneralConfig = reqGeneral("buymileage.limit.period.start.date", identity);
                BaseResponse<List<GeneralConfiguration>> generalConfig = generalConfigurationClient.retrieve(reqGeneralConfig);
                if (generalConfig.getResult().size() < 0) {
                    throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "General Config", "buymileage.limit.period.start.date")));
                }
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String startdate = generalConfig.getResult().get(0).getValue();
                Date dateStart = format.parse(startdate);
                buyMileageLimit.setStartdate(dateStart);
            }
        }

        if (buyMileageLimit.getEnddate() == null) {
            // check general config
            BaseRequest<BaseParameter> reqGeneral = reqGeneral("buymileage.limit.period.end", identity);
            BaseResponse<List<GeneralConfiguration>> general = generalConfigurationClient.retrieve(reqGeneral);
            if (general.getResult().size() < 0) {
                throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "General Config", "buymileage.limit.period.end")));
            }
            if (general.getResult().get(0).getValue().equals("YEAR_END")) {
                Date now = new Date();
                Calendar cal = Calendar.getInstance();
                int year = Calendar.getInstance().get(Calendar.YEAR);
                cal.set(year, 11, 31);
                now = cal.getTime();
                buyMileageLimit.setEnddate(now);
            } else if (general.getResult().get(0).getValue().equals("DATE")) {
                // check general config
                BaseRequest<BaseParameter> reqGeneralConfig = reqGeneral("buymileage.limit.period.end.date", identity);
                BaseResponse<List<GeneralConfiguration>> generalConfig = generalConfigurationClient.retrieve(reqGeneralConfig);
                if (generalConfig.getResult().size() < 0) {
                    throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "General Config", "buymileage.limit.period.end.date")));
                }
                String enddate = generalConfig.getResult().get(0).getValue();
                Date dateEnd = new SimpleDateFormat("yyyy-MM-dd").parse(enddate);
                buyMileageLimit.setEnddate(dateEnd);
            }
        }
        if (buyMileageLimit.getEnddate().before(buyMileageLimit.getStartdate())) {
            throw new ApplicationException(Status.INVALID(Translator.get("data.date.validation")));
        }

        List<BuyMileageLimit> buyMileageLimitList = buyMileageLimitRepository.findByMemberid(buyMileageLimit.getMemberid());
        if (buyMileageLimitList.size() != 0) {
            for (BuyMileageLimit bml : buyMileageLimitList) {
                if (buyMileageLimit.getMemberid().equals(bml.getMemberid())) {
                    if ((!bml.getStartdate().after(buyMileageLimit.getStartdate()) && !bml.getEnddate().before(buyMileageLimit.getStartdate())) ||
                            (!bml.getStartdate().after(buyMileageLimit.getEnddate()) && !bml.getEnddate().before(buyMileageLimit.getEnddate())) ||
                            (!bml.getStartdate().before(buyMileageLimit.getStartdate()) && !bml.getEnddate().after(buyMileageLimit.getEnddate()))) {
                        throw new ApplicationException("data.already.exist", "Buy Mileage Limit with memberid ", buyMileageLimit.getMemberid());
                    }
                }
            }
        }

        if (buyMileageLimit.getAllowedmileage() == null) {
            // check general config
            BaseRequest<BaseParameter> reqGeneralConfig = reqGeneral("buymileage.limit.defaultmaxmileage", identity);
            BaseResponse<List<GeneralConfiguration>> generalConfig = generalConfigurationClient.retrieve(reqGeneralConfig);
            if (generalConfig.getResult().size() < 0) {
                throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "General Config", "buymileage.limit.defaultmaxmileage")));
            }
            String mileage = generalConfig.getResult().get(0).getValue();
            BigInteger allowedMileage = new BigInteger(mileage);
            buyMileageLimit.setAllowedmileage(allowedMileage);
        }
        if (buyMileageLimit.getTotalmileage() == null) {
            buyMileageLimit.setTotalmileage(BigInteger.valueOf(0));
        }
        buyMileageLimit.setMemberbuymileagelimitid(memberbuymileagelimitid);
        buyMileageLimit.setCreatedBy(identity.getUserid());
        super.save(buyMileageLimit);
        return buyMileageLimit;
    }

    @Override
    public BuyMileageLimit update(BuyMileageLimit buyMileageLimit, Identity identity) {
        BuyMileageLimit buyMileageLimitDB = buyMileageLimitRepository.findById(buyMileageLimit.getMemberbuymileagelimitid()).orElse(null);
        if (buyMileageLimitDB == null) {
            throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Buy Mileage Limit", buyMileageLimit.getMemberbuymileagelimitid())));
        }
        // check member
        BaseRequest<BaseParameter<Member>> reqMember = reqMember(buyMileageLimit.getMemberid(), identity);
        BaseResponse<Member> member = memberProfileClient.profile(reqMember);
        if (member == null) {
            throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Member", buyMileageLimit.getMemberid())));
        }
        if (buyMileageLimit.getEnddate().before(buyMileageLimit.getStartdate())) {
            throw new ApplicationException(Status.INVALID(Translator.get("data.date.validation")));
        }
        List<BuyMileageLimit> buyMileageLimitList = buyMileageLimitRepository.findByMemberid(buyMileageLimit.getMemberid());
        if (buyMileageLimitList.size() != 0) {
            for (BuyMileageLimit bml : buyMileageLimitList) {
                if (!bml.getMemberbuymileagelimitid().equals(buyMileageLimit.getMemberbuymileagelimitid())) {
                    if ((!bml.getStartdate().after(buyMileageLimit.getStartdate()) && !bml.getEnddate().before(buyMileageLimit.getStartdate())) ||
                            (!bml.getStartdate().after(buyMileageLimit.getEnddate()) && !bml.getEnddate().before(buyMileageLimit.getEnddate())) ||
                            (!bml.getStartdate().before(buyMileageLimit.getStartdate()) && !bml.getEnddate().after(buyMileageLimit.getEnddate()))) {
                        throw new ApplicationException("data.already.exist", "Buy Mileage Limit with memberid ", buyMileageLimit.getMemberid());
                    }
                }
            }
        }
        buyMileageLimit.setUpdatedBy(identity.getUserid());
        buyMileageLimit.setCreatedBy(buyMileageLimitDB.getCreatedBy());
        super.update(buyMileageLimit);
        return buyMileageLimit;
    }

    @Override
    public BuyMileageLimit collect(BuyMileageLimit buyMileageLimit, Identity identity) throws ParseException {
        // check member
        BaseRequest<BaseParameter<Member>> reqMember = reqMember(buyMileageLimit.getMemberid(), identity);
        BaseResponse<Member> member = memberProfileClient.profile(reqMember);
        if (member == null) {
            throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Member", buyMileageLimit.getMemberid())));
        }

        List<BuyMileageLimit> buyMileageLimitList = buyMileageLimitRepository.findByMemberid(buyMileageLimit.getMemberid());
        List<BuyMileageLimit> buyMileageLimits = new ArrayList<>();
        BuyMileageLimit collect = new BuyMileageLimit();
        if (buyMileageLimitList.size() == 0) {
            buyMileageLimit.setTotalmileage(buyMileageLimit.getMileage());
            collect = save(buyMileageLimit, identity);
        } else {
            Date now = new Date();
            if (buyMileageLimitList.size() != 0) {
                for (BuyMileageLimit bml : buyMileageLimitList) {
                    if ((!now.after(bml.getStartdate()) && !now.before(bml.getStartdate())) ||
                            (!now.after(bml.getEnddate()) && !now.before(bml.getEnddate())) ||
                            (!now.before(bml.getStartdate()) && !now.after(bml.getEnddate()))) {
                        buyMileageLimits.add(bml);
                    }
                }
            }
            if (buyMileageLimits.size() == 0) {
                buyMileageLimit.setTotalmileage(buyMileageLimit.getMileage());
                collect = save(buyMileageLimit, identity);
            } else {
                collect = buyMileageLimits.get(0);
                int totalmileage = collect.getTotalmileage().intValue() + buyMileageLimit.getMileage().intValue();
                int allowedmileage = collect.getAllowedmileage().intValue();
                if (totalmileage > allowedmileage) {
                    throw new ApplicationException(Status.INVALID("Buy Mileage Limit already exeed"));
                }
                collect.setMileage(null);
                collect.setTotalmileage(BigInteger.valueOf(totalmileage));
                collect = update(collect, identity);
            }
        }
        return collect;
    }

    @Override
    public BuyMileageLimit check(BuyMileageLimit buyMileageLimit, Identity identity) {
        // check member
        BaseRequest<BaseParameter<Member>> reqMember = reqMember(buyMileageLimit.getMemberid(), identity);
        BaseResponse<Member> member = memberProfileClient.profile(reqMember);
        if (member == null) {
            throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Member", buyMileageLimit.getMemberid())));
        }

        List<BuyMileageLimit> buyMileageLimitList = buyMileageLimitRepository.findByMemberid(buyMileageLimit.getMemberid());
        BuyMileageLimit buyMileageLimitCheck = new BuyMileageLimit();
        if (buyMileageLimitList.size() == 0) {
            throw new ApplicationException(Status.DATA_NOT_FOUND(Translator.get("data.not.found", "Buy Mileage Limit with memberid ", buyMileageLimit.getMemberid())));
        }
        Date now = new Date();
        if (buyMileageLimitList.size() != 0) {
            for (BuyMileageLimit bml : buyMileageLimitList) {
                if ((!now.after(bml.getStartdate()) && !now.before(bml.getStartdate())) ||
                        (!now.after(bml.getEnddate()) && !now.before(bml.getEnddate())) ||
                        (!now.before(bml.getStartdate()) && !now.after(bml.getEnddate()))) {
                    buyMileageLimitCheck = bml;
                }
            }
        }
        return buyMileageLimitCheck;
    }

    @Override
    public void delete(BuyMileageLimit buyMileageLimit) {
        BuyMileageLimit buyMileageLimitDB = buyMileageLimitRepository.findById(buyMileageLimit.getMemberbuymileagelimitid()).orElse(null);

        if (buyMileageLimitDB != null) {
            try {
                this.delete(buyMileageLimit.getMemberbuymileagelimitid());
            } catch (Exception e) {
                throw new ApplicationException("data.not.delete", "Buy Mileage Limit", buyMileageLimit.getMemberbuymileagelimitid());
            }
        } else {
            throw new ApplicationException("data.not.found", "Buy Mileage Limit", buyMileageLimit.getMemberbuymileagelimitid());
        }

    }

    public BaseRequest reqMember(String memberid, Identity identity) {
        BaseParameter<Member> parameter = new BaseParameter<>();
        BaseRequest<BaseParameter> request = new BaseRequest<>();
        Member member = new Member();
        member.setMemberid(memberid);
        parameter.setData(member);
        request.setParameter(parameter);
        request.setIdentity(identity);
        return request;
    }

    public BaseRequest reqGeneral(String key, Identity identity) {
        BaseParameter<GeneralConfiguration> parameter = new BaseParameter<>();
        BaseRequest<BaseParameter> request = new BaseRequest<>();
        parameter.addCriteria("key", key);
        request.setParameter(parameter);
        request.setIdentity(identity);
        return request;
    }
}
