package id.co.asyst.amala.member.buymileage.validator.buymileage;

import id.co.asyst.amala.member.model.BuyMileage;
import id.co.asyst.amala.member.model.BuyMileageStatusEnum;
import id.co.asyst.amala.member.model.SourceEnum;
import id.co.asyst.commons.core.Constants;
import id.co.asyst.commons.core.exception.ApplicationException;
import id.co.asyst.commons.core.payload.BaseParameter;
import id.co.asyst.commons.core.payload.BaseRequest;
import id.co.asyst.commons.core.payload.Status;
import id.co.asyst.commons.core.validator.BaseValidator;
import id.co.asyst.commons.core.validator.ValidatorType;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class BuyMileageValidator extends BaseValidator<BuyMileage> {
    private static final String regexEmail = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    public void validate(BaseParameter<BuyMileage> parameter) {
        validate(parameter, BuyMileage.class);
    }

    public void validate(BaseRequest<BaseParameter<BuyMileage>> request, ValidatorType type) {
        notNull(request.getIdentity(), "identity");
        super.validate(request.getParameter(), BuyMileage.class);
        if (type.equals(ValidatorType.CREATE)
                || type.equals(ValidatorType.RETRIEVEDETAIL)
                || type.equals(ValidatorType.ACTIVATE)
                || type.equals(ValidatorType.DEACTIVATE)
        ) {
            notNull(request.getParameter(), "parameter");
            notNull(request.getParameter().getData(), "data");
        }
        BuyMileage buyMileage = request.getParameter().getData();
        if (!ValidatorType.CREATE.equals(type)) {
            if (!ValidatorType.UPDATE.equals(type)) {
                if (ValidatorType.RETRIEVEDETAIL.equals(type) || ValidatorType.DELETE.equals(type)) {
                    checkMemberBuyMileageId(buyMileage.getMemberbuymileageid());
                    return;
                }
            }
            checkMemberBuyMileageId(buyMileage.getMemberbuymileageid());
            validation(buyMileage);
            return;
        }

        validation(buyMileage);
    }

    private void validation(BuyMileage buyMileage) {
        checkBuyMileageId(buyMileage.getBuymileageid());
        checkMemberId(buyMileage.getMemberid());
        checkBuyDate(buyMileage.getBuydate());
        checkMemberPrimaryEmail(buyMileage.getMemberprimaryemail());
        checkSource(buyMileage.getSource());
        checkQty(buyMileage.getQty());
        checkCurrencyCode(buyMileage.getCurrencycode());
        checkPrice(buyMileage.getPrice());
        checkCataloguePrice(buyMileage.getCatalogueprice());
        checkVat(buyMileage.getVat());
        checkNettAmount(buyMileage.getNettamount());
        checkVatAmount(buyMileage.getVatamount());
        checkStatus(buyMileage.getStatus());
        checkTotalPrice(buyMileage.getTotalprice());
        checkIncludevat(buyMileage.getIncludevat());
        checkTotalAmount(buyMileage.getTotalamount());

        if (buyMileage.getSource().equals(SourceEnum.PARTNER)) {
            notBlank(buyMileage.getPartnercode(), "partnercode");
            isMax(buyMileage.getPartnercode(), 20, "partnercode");
        }
    }


    private void checkMemberBuyMileageId(String memberbuymileageid) {
        notBlank(memberbuymileageid, "memberbuymileageid");
        isMax(memberbuymileageid, 20, "memberbuymileageid");
        isAlphanumeric(memberbuymileageid, "memberbuymileageid");
    }

    private void checkBuyMileageId(String buymileageid) {
        notBlank(buymileageid, "buymileageid");
        isMax(buymileageid, 20, "buymileageid");
        isAlphanumeric(buymileageid, "buymileageid");
    }

    private void checkMemberId(String memberid) {
        notBlank(memberid, "memberid");
        isMax(memberid, 20, "memberid");
        isAlphanumeric(memberid, "memberid");
    }

    private void checkBuyDate(Date buydate) {
        notNull(buydate, "buydate");
    }

    private void checkMemberPrimaryEmail(String memberprimaryemail) {
        notBlank(memberprimaryemail, "memberprimaryemail");
        isMax(memberprimaryemail, 45, "memberprimaryemail");
        if (!memberprimaryemail.matches(Constants._REGEX_EMAIL)) {
            throw new ApplicationException(Status.INVALID("invalid email format"));
        }
    }

    private void checkSource(SourceEnum source) {
        notNull(source, "source");
        if (!source.toString().equalsIgnoreCase(SourceEnum.TO.toString()) && !source.toString().equalsIgnoreCase(SourceEnum.PARTNER.toString())) {
            throw new ApplicationException(Status.INVALID("source field is required"));
        }
    }

    private void checkQty(Integer qty) {
        notNull(qty, "qty");
    }

    private void checkCurrencyCode(String currencycode) {
        notBlank(currencycode, "currencycode");
        isMax(currencycode, 10, "currencycode");
        isAlphanumeric(currencycode, "currencycode");
    }

    private void checkPrice(Double price) {
        notNull(price, "price");
    }

    private void checkCataloguePrice(Double catalogueprice) {
        notNull(catalogueprice, "catalogueprice");
    }

    private void checkNettAmount(Double nettamount) {
        notNull(nettamount, "nettamount");
    }

    private void checkIncludevat(Boolean includevat) {
        notNull(includevat, "includevat");
    }

    private void checkVatAmount(Double vatamount) {
        notNull(vatamount, "vatamount");
    }

    private void checkTotalPrice(Double totalprice) {
        notNull(totalprice, "totalprice");
    }

    private void checkTotalAmount(Double totalamount) {
        notNull(totalamount, "totalamount");
    }


    private void checkVat(Integer vat) {
        notNull(vat, "vat");
    }

    private void checkStatus(BuyMileageStatusEnum status) {
        notNull(status, "status");
        if (!status.toString().equalsIgnoreCase(BuyMileageStatusEnum.SUCCESS.toString()) && !status.toString().equalsIgnoreCase(BuyMileageStatusEnum.FAILED.toString()) &&
                !status.toString().equalsIgnoreCase(BuyMileageStatusEnum.WAITING_FOR_PAYMENT.toString())) {
            throw new ApplicationException(Status.INVALID("status field is required"));
        }
    }

}