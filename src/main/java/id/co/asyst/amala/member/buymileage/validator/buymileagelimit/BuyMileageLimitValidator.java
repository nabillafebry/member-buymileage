package id.co.asyst.amala.member.buymileage.validator.buymileagelimit;

import id.co.asyst.amala.member.model.BuyMileageLimit;
import id.co.asyst.commons.core.payload.BaseParameter;
import id.co.asyst.commons.core.payload.BaseRequest;
import id.co.asyst.commons.core.validator.BaseValidator;
import id.co.asyst.commons.core.validator.ValidatorType;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Date;

@Component
public class BuyMileageLimitValidator extends BaseValidator<BuyMileageLimit> {

    public void validate(BaseParameter<BuyMileageLimit> parameter) {
        validate(parameter, BuyMileageLimit.class);
    }

    public void validateCollect(BaseRequest<BaseParameter<BuyMileageLimit>> request) {
        notNull(request.getIdentity(), "identity");
        super.validate(request.getParameter(), BuyMileageLimit.class);
        BuyMileageLimit buyMileageLimit = request.getParameter().getData();
        notBlank(buyMileageLimit.getMemberid(), "memberid");
        notNull(buyMileageLimit.getMileage(), "mileage");
    }

    public void validateCheck(BaseRequest<BaseParameter<BuyMileageLimit>> request) {
        notNull(request.getIdentity(), "identity");
        super.validate(request.getParameter(), BuyMileageLimit.class);
        BuyMileageLimit buyMileageLimit = request.getParameter().getData();
        notBlank(buyMileageLimit.getMemberid(), "memberid");
    }

    public void validate(BaseRequest<BaseParameter<BuyMileageLimit>> request, ValidatorType type) {
        notNull(request.getIdentity(), "identity");
        super.validate(request.getParameter(), BuyMileageLimit.class);
        if (type.equals(ValidatorType.CREATE)
                || type.equals(ValidatorType.RETRIEVEDETAIL)
                || type.equals(ValidatorType.ACTIVATE)
                || type.equals(ValidatorType.DEACTIVATE)
        ) {
            notNull(request.getParameter(), "parameter");
            notNull(request.getParameter().getData(), "data");
        }
        BuyMileageLimit buyMileageLimit = request.getParameter().getData();
        if (type.equals(ValidatorType.UPDATE)
                || type.equals(ValidatorType.RETRIEVEDETAIL)
                || type.equals(ValidatorType.DELETE)) {
            notBlank(buyMileageLimit.getMemberbuymileagelimitid(), "memberbuymileagelimitid");
        }

        if (type.equals(ValidatorType.UPDATE) || type.equals(ValidatorType.CREATE)) {
            checkMemberid(buyMileageLimit.getMemberid());
            if (type.equals(ValidatorType.UPDATE)) {
                checkAllowedMileage(buyMileageLimit.getAllowedmileage());
                checkTotalMileage(buyMileageLimit.getTotalmileage());
                checkStartDate(buyMileageLimit.getStartdate());
                checkEndDate(buyMileageLimit.getEnddate());
            }
        }
    }

    private void checkTotalMileage(BigInteger totalmileage) {
        notNull(totalmileage, "totalmileage");
    }

    private void checkAllowedMileage(BigInteger allowedmileage) {
        notNull(allowedmileage, "allowedmileage");
    }

    private void checkEndDate(Date enddate) {
        notNull(enddate, "enddate");
    }

    private void checkStartDate(Date startdate) {
        notNull(startdate, "startdate");
    }

    private void checkMemberid(String memberid) {
        notBlank(memberid, "memberid");
        isMax(memberid, 20, "memberid");
    }
}
